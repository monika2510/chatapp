laravel 8 chat application
Requirement as below:
Please see coding challenge below, Coding Challenge Design a messaging system using Laravel 8. The system should be able to view, send, receive, and delete messages between various users. Restrictions on this you should use tailwind css for any css styling. Use blade templates and partials to their full effect. Also follow DRY and KISS principles. Deliverables: Git repo including instructions on how to install and run the application in a README file. Additional notes: The test is intentionally not pre-scaffolded to test that you are comfortable setting up and packing a solution. Completing the project is important and is for us to see your own coding style, we are not looking for a carbon copy of an existing solution.
Follow installation steps:
Fire commands in terminal :

Step : 1
git clone https://gitlab.com/monika2510/chatapp

Step : 2
cd  chatapp

Step 3
change .env.example to .env
Create "chatapp" db on your system.

Step : 4
composer install

Step : 5
php artisan migrate
This will generate all tables in db.

step : 6
hit this command php artisan serve

step : 7
http://127.0.0.1:8000/ run project to this url

step : 8
hit this command php artisan key:generate

step : 9
after all the process register the user and login